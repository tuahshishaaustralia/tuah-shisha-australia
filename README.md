Whether you’re based in Melbourne, Perth, Brisbane, Sydney or Gold Coast – We’ll deliver directly to your door.
Do you want to experience your first Shisha, or try your first Tuah shisha flavour, but don’t know how to select the best flavour, where to buy quality coal, or even how to put together your first Shisha.

Address: 180 York St, Port Melbourne, VIC 3207, Australia

Phone: +61 431 379 161

Website: https://www.ttlcherbal.com/
